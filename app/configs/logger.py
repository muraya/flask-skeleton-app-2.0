import logging

logger = logging.getLogger("Car Wash API")
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('/var/log/applications/python/carwash/logs.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - FileName: %(filename)s - LineNo: %(lineno)s -'
    ' FunctionName: %(funcName)20s() - Message: %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)
