from orator import Model
from app import db

Model.set_connection_resolver(db)


class Users(Model):
    __table__ = 'users'
    __guarded__ = ['id']

    # def check_password(self, password):
    #    return flask_bcrypt.check_password_hash(self.password_hash, password)

    pass
