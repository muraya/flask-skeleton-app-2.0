from orator import Model

from app import db

Model.set_connection_resolver(db)


class RevokedTokens(Model):
    __table__ = 'revoked_tokens'
    __guarded__ = ['id']
    pass
