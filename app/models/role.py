from orator import Model
from app import db

Model.set_connection_resolver(db)


class Roles(Model):
    __table__ = 'roles'
    __guarded__ = ['id']
    pass
