from flask import jsonify

from app.configs import logger
from app.models.business import Business
from app.models.role import Roles
from app.models.user import Users


class CoreUtilities:
    @staticmethod
    def current_user(phone):
        try:
            return Users.join('roles', 'roles.id', '=', 'users.role_id') \
                .join('head_quarters', 'head_quarters.user_id', '=', 'users.id') \
                .left_join('business_users', 'business_users.user_id', '=', 'users.id') \
                .left_join('businesses', 'businesses.id', '=', 'business_users.business_id') \
                .where('users.phone_number', phone) \
                .select('users.*', 'roles.role_code', 'head_quarters.id as head_quarters_id', 'businesses.id').first()
        except Exception as err:
            logger.logger.error(str(err))
            return False

    @staticmethod
    def fetch_roles():
        try:
            return Roles.select('role_name', 'id').get()
        except Exception as err:
            logger.logger.error(str(err))
            return False
